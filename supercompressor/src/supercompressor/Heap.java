package supercompressor;

// Heap.java
public class Heap {

  private BinaryTree v[];
  private int size;

  public Heap() {
    size = 0;
    v = new BinaryTree[12000];
  }

  private int left ( int i )   { return 2 * i + 1; }
  private int right ( int i )  { return 2 * i + 2; }
  private int parent ( int i ) { return (i-1) / 2; }

  private void sift_up ( int pos ) {

    int p = parent ( pos );

    if ( p == pos ) return;

    if ( v[p].getData() > v[pos].getData() ) {
      BinaryTree tmp = v[p];
      v[p] = v[pos];
      v[pos] = tmp;
      sift_up ( p );
    }
  }

  public void insert( BinaryTree bintree ) {
    v[size] = bintree;
    sift_up( size );
    size++;
  }

  private void sift_down ( int pos ) {
   int pleft = left(pos);
   int pright = right(pos);
   int pmenor = pos;
   if(pleft < size && v[pleft].getData() < v[pmenor].getData()) pmenor = pleft;
   if(pright < size && v[pright].getData() < v[pmenor].getData()) pmenor = pright;
   
   if(pos != pmenor){
       BinaryTree aux = v[pos];
       v[pos] = v[pmenor];
       v[pmenor] = aux;
        sift_down(pmenor);
    }
  }

  public BinaryTree get( ) {
    BinaryTree res = v[0];
    if (size > 0)
    v[0] = v[--size];
    sift_down( 0 );
    return res;
  }

  private void print( int b, int elem, int sp )  {
    int i, j;

    System.out.println( "" );
    for( j = 0; j < size; j++ ) System.out.print( v[j] + " " );
    System.out.println( "" );

    while ( true ) {
      for( j = 0; j <= sp / 2; j++ ) System.out.print( " " );
      for( i = b; i < b + elem; i++ ) {
        if ( i == size ) return;
        System.out.print( v[i] );
        for( j = 0; j < sp; j++ ) System.out.print( " " );
      }
      System.out.println( "" );
      b = b + elem;
      elem = 2 * elem;
      sp = sp / 2;
    }
  }
  public int size(){
	  return size;
  }
  public void print( )  {
    System.out.println( "" );
    print( 0, 1, 32 );
    System.out.println( "" );
  }

  /*public void altera(int a, int b){
      for(int i = 0; i < size; i++){
          if(v[i] == a){
              v[i] = b;
              if(a < b) sift_down(i);
              else sift_up(i);
              return;
            }
        }      
        
    }*/
    
  //public void ordena
}
