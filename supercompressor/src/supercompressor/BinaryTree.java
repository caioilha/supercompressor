package supercompressor;

import java.util.ArrayList;
import java.util.HashMap;

public class BinaryTree {
	private Node root;

	private static class Node {
		Node left;
		Node right;
		int data;
		Character carac;
		String codigo;

		Node() {
			left = right = null;
		}
		Node (int data){
			left = right = null;
			this.data = data;
		}
	}

	/* Cria uma �rvore vazia, ou seja, uma refer�ncia nula. */
	public BinaryTree() {
		root = null;
	}
	public BinaryTree(Character cci){
		Node pastel = new Node();
		pastel.carac = cci;
		pastel.data = 0;
		pastel.left = null;
		pastel.right = null;
		root = pastel;
	}
	/*public static int menorValor(ArrayList<BinaryTree> BinTrees){
		int menor = Integer.MAX_VALUE;
		int menorIndice = 0;
		for (int i = 0; i < BinTrees.size(); i++){
			CharComInt tantofaz = BinTrees.get(i).getCharint();
			int valor = BinTrees.get(i).root.data;
			if(tantofaz != null) valor = tantofaz.getN(); 
			if (valor <= menor){
				menor = valor; menorIndice = i;
			}
		}
		return menorIndice;
	}*/
	/*public static int valorReal(BinaryTree qualquerCoisa){
		int valor = qualquerCoisa.root.data;
		if (qualquerCoisa.getCharint() != null)
			valor = qualquerCoisa.getCharint().getN();
		return valor;
	}*/
	//a cagada provavelmente t� no JuntaTrees
	public static BinaryTree JuntaTrees(Heap heap){
		while (heap.size() > 1){
			BinaryTree menor = heap.get();
			BinaryTree maior = heap.get();
			BinaryTree nova = new BinaryTree(null);
			nova.root.left = menor.root;
			nova.root.right = maior.root;
			nova.root.data = menor.root.data + maior.root.data;
			heap.insert(nova);
			//add nova no heap
		
		}
		return heap.get();
	}
	public char getC(){
		return root.carac;
	}
	public void printPodre(){
		System.out.println(root.carac +": " + root.data);
	}
	public int getData(){
		return root.data;
	}
	public void print(){
		print(root, "");
	}
	private void print (Node p, String t){
		if (p == null) return;
		print(p.right, t + "\t");
		if(p.carac != null){
			System.out.println(t +" " +  p.carac + " " + p.data);
		}
		else
		System.out.println(t + p.data);
		print(p.left, t + "\t");
	}
	public void codigoDe(HashMap<Character, String> map){
		codigoDe(root,"", map);
	}
	//filohs da direita recebem 1
	private void codigoDe(Node n, String s, HashMap<Character, String> map){
		if (n == null) return;
		if (n.carac != null) map.put(n.carac, s);
		codigoDe(n.left, s + "0", map);
		codigoDe(n.right, s + "1", map);
	}
	public String getCod(){
		return root.codigo;
	}
	public void setCod(String cod){
		root.codigo = cod;
	}
	public void addData(){
		root.data = root.data + 1;
	}
}
