package supercompressor;

import java.io.*;

import java.nio.charset.Charset;
import java.util.*;
import java.util.HashMap;
import java.util.Collections;


public class app {
	public static void main(String args[]){
		long tart = System.currentTimeMillis();
		String [] strings ={"arq1.txt", "arq2.txt", "arq3.txt", "arq4.txt",
				"arq5.txt", "arq6.pdf", "arq7.html", "arq8.html", "arq9.html", "arq10.html"};
		for (String arq: strings){

			/////////parte q faz a contagem de caracteres
			ArrayList<BinaryTree> BinTrees = new ArrayList<>();
			//HashMap<Character, BinaryTree> semiLista = new HashMap<>();
			//System.out.println("lewndo os caracterews");

			try{
				BufferedReader reader = new BufferedReader
						(new InputStreamReader
								(new FileInputStream(arq),Charset.forName("UTF-8")));

				int w;
				while((w = reader.read()) != -1) {
					char c = (char) w;
					int in = in(BinTrees, c);
					if (in == -1){
						BinaryTree aa = new BinaryTree(c);
						aa.addData();
						BinTrees.add(aa);
					}
					else BinTrees.get(in).addData();

				}
				reader.close();
			} catch(Exception e){
				System.out.println("error");
			}
			//lambda is gudd
			//semiLista.forEach( (k,v) -> BinTrees.add(v));

			/*for (Map.Entry<Character, BinaryTree> entry : semiLista.entrySet()){
		    BinTrees.add(entry.getValue());
		}*/
			Heap heap = new Heap();
			for (BinaryTree bb: BinTrees)
				heap.insert(bb);
			long start = System.currentTimeMillis();
			BinaryTree uma = BinaryTree.JuntaTrees(heap);
			long elapsedTimeMillis = System.currentTimeMillis()-start;
			//	System.out.println(elapsedTimeMillis);
			//uma.print();
			//System.out.println("");
			start = System.currentTimeMillis();
			//for(int i = 0; i < BinTrees.size(); i++){
			//BinTrees.get(i).setCod(uma.codigoDe(BinTrees.get(i).getC()));

			//System.out.println(BinTrees.get(i).getC() + " possui cod " + BinTrees.get(i).getCod());
			//}
			HashMap<Character, String> mapCod = new HashMap<Character, String>();
			uma.codigoDe(mapCod);
			elapsedTimeMillis = System.currentTimeMillis()-start;
			//System.out.println(elapsedTimeMillis);

			///////////coidifica��o
			//System.out.println("codificanod o baguio");

			//for(BinaryTree bt: BinTrees){
			//mapCod.put(bt.getC(), bt.getCod());
			//}
			try{
				PrintWriter writer = new PrintWriter(arq+".coo", "UTF-8");
				//////escreve o char e o codigo dele  aqui p poder recriar a arvore binaria

				//////escreve o char e o codigo dele aqui p poder recriar a arvore binaria
				BufferedReader reader1 = new BufferedReader
						(new InputStreamReader
								(new FileInputStream(arq),Charset.forName("UTF-8")));

				int w;
				while((w = reader1.read()) != -1) {
					char c = (char) w;
					writer.write(mapCod.get(c));
				}
				writer.close();
				reader1.close();
			} catch(Exception e){
				System.out.println("error");
			}
			////codifica��o
			/////decodifica��o

			//System.out.println("defocidifano o baugui");
			HashMap<String, Character> mapDec = new HashMap<String, Character>();
			for(BinaryTree bt: BinTrees){
				mapDec.put(bt.getCod(), bt.getC());
			}
			mapCod.forEach( (k,v) -> mapDec.put(v, k));
			try{
				BufferedReader reader2 = new BufferedReader
						(new InputStreamReader
								(new FileInputStream(arq+ ".coo"),Charset.forName("UTF-8")));
				//tentar usar bufferedwriter com um filewriter
				// BufferedWriter writer2 = new BufferedWriter(new FileWriter());
				PrintWriter writer2 = new PrintWriter(arq +" decodificado", "UTF-8");
				String codigo = "";
				int w;
				while((w = reader2.read()) != -1) {
					char c = (char) w;
					codigo += c;
					if(mapDec.containsKey(codigo)){
						writer2.write(mapDec.get(codigo));
						codigo = "";
					}
				}
				reader2.close();
				writer2.close();
			} catch(Exception e){
				System.out.println("deu pau na decodifica��o");
			}
			////decodifica��o
			System.out.println(arq + " possui " + contaCaracteres(arq) + " bytes");
			System.out.println(arq + "codificado possui: " + contaCaracteres(arq + ".coo") + " bits");
			System.out.println("acabou o bagui do " + arq);
			System.out.println(arq + "possui diferen�as? Descubra a seguir: " + diferen�a(arq, arq +" decodificado" ));
		}
		long fims = System.currentTimeMillis() - tart;
		System.out.println("execucao levou: "  + fims/1000 + " segundos");
	}
	public static int in(ArrayList<BinaryTree> vetor, char c){//verifica se um caractere ja est� la
		for(int i = 0; i < vetor.size(); i++){
			if(vetor.get(i).getC() == c)
				return i;
		}
		return -1;
	}
	public static int contaCaracteres(String arq){
		int i = 0;
		try{
			BufferedReader reader = new BufferedReader
					(new InputStreamReader
							(new FileInputStream(arq),Charset.forName("UTF-8")));
			int w;
			while((w = reader.read()) != -1) {
				char c = (char) w;
				i++;
			}
			reader.close();
		} catch(Exception e){
			System.out.println("error");
		}
		return i;
	}
	public static boolean diferen�a(String arq, String arqDecod){
		try {
			BufferedReader reader1 = new BufferedReader
					(new InputStreamReader
							(new FileInputStream(arq),Charset.forName("UTF-8")));
			BufferedReader reader2 = new BufferedReader
					(new InputStreamReader
							(new FileInputStream(arqDecod),Charset.forName("UTF-8")));
			int w;
			int k;
			while((w = reader1.read()) != -1 && (k = reader2.read()) != -1) {
				char c = (char) w;
				char d = (char) k;
				if (c != d) return true;
			}
			reader1.close();
			reader2.close();
		} catch (Exception e ) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return false;
	}

}
